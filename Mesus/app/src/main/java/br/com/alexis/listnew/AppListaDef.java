package br.com.alexis.listnew;


/**
 * Created by T-Gamer on 27/10/2016.
 */

public class AppListaDef {

    private String textoBotao;
    private int imagem;

    public AppListaDef(String textoBotao, int imagem) {
        this.textoBotao = textoBotao;
        this.imagem = imagem;
    }


    public String getTextoBotao() {
        return textoBotao;
    }

    public void setTextoBotao(String textoBotao) {
        this.textoBotao = textoBotao;
    }


    public int getImagem() {
        return imagem;
    }

    public void setImagem(int imagem) {
        this.imagem = imagem;
    }


    @Override
    public String toString() {
        return textoBotao;
    }
}
