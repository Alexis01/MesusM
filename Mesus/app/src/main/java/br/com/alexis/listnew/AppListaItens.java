package br.com.alexis.listnew;

import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

public class AppListaItens extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.def_list_app);

        List<AppListaDef> appListDefs = gerarLista(); // new ArrayList<Zombie>();//

        final AppListaAdapter appListaAdapter = new AppListaAdapter(this, appListDefs);
        setListAdapter(appListaAdapter);


        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AppListaDef appListaDef = appListaAdapter.getItem(position);
                Toast.makeText(AppListaItens.this,appListaDef.getTextoBotao(), Toast.LENGTH_SHORT).show();
                if ( appListaDef.getTextoBotao() == "Doar") {
                    startActivity(new Intent(AppListaItens.this, MapsActivity.class));
                } else if (appListaDef.getTextoBotao() == "Receber") {
                    startActivity(new Intent(AppListaItens.this, LoginActivity.class));
                } else if (appListaDef.getTextoBotao() == "Saiba mais") {
                    startActivity(new Intent(AppListaItens.this, ScrollingActivity.class));
                }
            }
        });

    }

    private List<AppListaDef> gerarLista() {
        List<AppListaDef> appListaDefs = new ArrayList<AppListaDef>();
        appListaDefs.add(criarItem("Doar", R.drawable.doar_image));
        appListaDefs.add(criarItem("Receber", R.drawable.receber_image));
        appListaDefs.add(criarItem("Saiba mais", R.drawable.play_image));
        return appListaDefs;
    }

    private AppListaDef criarItem(String nome, int image) {
        AppListaDef appListaDef = new AppListaDef(nome, image);
        return appListaDef;
    }
}