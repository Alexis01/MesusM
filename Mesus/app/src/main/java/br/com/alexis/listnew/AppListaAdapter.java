package br.com.alexis.listnew;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by T-Gamer on 27/10/2016.
 */

public class AppListaAdapter extends ArrayAdapter<AppListaDef> {

    private Context context;
    private List<AppListaDef> appListaDefs = null;

    public AppListaAdapter(Context context, List<AppListaDef> appListaDefs) {
        super(context,0, appListaDefs);
        this.appListaDefs = appListaDefs;
        this.context = context;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        AppListaDef appListaDef = appListaDefs.get(position);

        if(view == null)
            view = LayoutInflater.from(context).inflate(R.layout.item_list_app, null);

        ImageView imageView = (ImageView) view.findViewById(R.id.image);
        imageView.setImageResource(appListaDef.getImagem());

        TextView textView = (TextView) view.findViewById(R.id.text_view_textBotao);
        textView.setText(appListaDef.getTextoBotao());

        return view;
    }
}
