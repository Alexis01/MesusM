package br.com.alexis.listnew;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ScrollingActivity extends ListActivity /*implements View.OnClickListener*/ {
    //ImageView imgClick;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        //imgClick = (ImageView) findViewById(R.id.imagePlay);

        //imgClick.setOnClickListener(this);
        List<AppListaDef> appListDefs = gerarLista(); // new ArrayList<Zombie>();//

        final AppListaAdapter appListaAdapter = new AppListaAdapter(this, appListDefs);
        setListAdapter(appListaAdapter);


        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AppListaDef appListaDef = appListaAdapter.getItem(position);
                Toast.makeText(ScrollingActivity.this,appListaDef.getTextoBotao(), Toast.LENGTH_SHORT).show();
                if ( appListaDef.getTextoBotao() == "Doar") {
                    startActivity(new Intent(ScrollingActivity.this, MapsActivity.class));
                } else if (appListaDef.getTextoBotao() == "Receber") {
                    startActivity(new Intent(ScrollingActivity.this, LoginActivity.class));
                } else if (appListaDef.getTextoBotao() == "Sobre") {
                    startActivity(new Intent(ScrollingActivity.this, Activity_Sobre.class));
                } else if (appListaDef.getTextoBotao() == "FAQ") {
                    startActivity(new Intent(ScrollingActivity.this, Activity_Faq.class));
                }
            }
        });
    }

//    @Override
//    public void onClick(View v) {
//        if (v.getId() == imgClick.getId()) {
//            Toast.makeText(this, imgClick.getParent().getClass().toString(), Toast.LENGTH_SHORT).show();
//        } else if ( v.getId() == imgClick.getId() ) {
//            Toast.makeText(this, imgClick.getParent().getClass().toString(), Toast.LENGTH_SHORT).show();
//        } else if ( v.getId() == imgClick.getId() ) {
//
//        }
//    }
    private List<AppListaDef> gerarLista() {
        List<AppListaDef> appListaDefs = new ArrayList<AppListaDef>();
        appListaDefs.add(criarItem("Doar", R.drawable.doar_image));
        appListaDefs.add(criarItem("Receber", R.drawable.receber_image));
        appListaDefs.add(criarItem("Sobre", R.drawable.exclama));
        appListaDefs.add(criarItem("FAQ", R.drawable.interroga));
        return appListaDefs;
    }

    private AppListaDef criarItem(String nome, int image) {
        AppListaDef appListaDef = new AppListaDef(nome, image);
        return appListaDef;
    }
}
